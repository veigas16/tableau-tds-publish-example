import requests
from requests.packages.urllib3.fields import RequestField
from requests.packages.urllib3.filepost import encode_multipart_formdata
import os
import json
import xml.etree.ElementTree as ET
from uuid import uuid4

# You'll need to update the constants below with valid values from your environment, I've
# hidden some info in environment vars simply to protect anything sensitive. Any value
# loaded from an environment var below is loaded as a string.

# Define DB constants
DB_SERVER =  os.environ.get('DB_SERVER') # The valid host address for your DB
DB_PORT = '3306'
DB_NAME = 'advisory_demo'
DB_TABLE = 'price_history'
DB_USER = os.environ.get('DB_USER') # The username used to login to the DB
DB_PASS = os.environ.get('DB_PASS') # The password used to login to the DB
DB_CLASS = 'mariadb' # relatively comprehensive list of valid classes: https://github.com/tableau/document-api-python/blob/master/tableaudocumentapi/dbclass.py

# Define Tableau Server constants
TS_USER = os.environ.get('TS_USER') # The username used to login to Tableau Server
TS_PASS = os.environ.get('TS_PASS') # The password used to login to Tableau Server
TS_BASE_URL = os.environ.get('TS_BASE_URL') # The URL used to access your Tableau Server with NO PATH or trailing slash
TS_API_VERSION = '3.5'
FILESIZE_LIMIT = 1024 * 1024 * 64
PROJ_ID = os.environ.get('TS_DEF_PROJ_ID') # The ID of the project your data source will be published to

def base36encode(number):
    """ Convert an integer to a base36 string """
    ALPHABET = "0123456789abcdefghijklmnopqrstuvwxyz"
    base36 = ''
    sign = ''
    if number < 0:
        sign = '-'
        number = -number
    if 0 <= number < len(ALPHABET):
        return sign + ALPHABET[number]
    while number != 0:
        number, i = divmod(number, len(ALPHABET))
        base36 = ALPHABET[i] + base36
    return sign + base36

def make_unique_name(dbclass):
    """ Provide a unique name for the dbclass """
    rand_part = base36encode(uuid4().int)
    name = dbclass + '.' + rand_part
    return name

def write_tds(server, port, dbname, username, dbclass, filename):
    """ 
        Write a minimally viable TDS file. Based on my testing you MUST include at
        least this much in your TDS to make it functional.
    """
    unique_dbclass = make_unique_name(dbclass)
    root = ET.Element('datasource', version='10.0', inline='true')
    outer_connection = ET.SubElement(root, 'connection')
    outer_connection.set('class', 'federated')
    named_conns = ET.SubElement(outer_connection, 'named-connections')
    nc = ET.SubElement(named_conns, 'named-connection', name=unique_dbclass, caption=server)
    conn = ET.SubElement(nc, 'connection', authentication='', port=port, dbname=dbname, server=server, username=username)
    conn.set('class', dbclass)
    relation = ET.SubElement(outer_connection, 'relation', connection=unique_dbclass, name=DB_TABLE, table='[{}]'.format(DB_TABLE), type='table')
    ET.ElementTree(root).write(filename, encoding='utf-8', xml_declaration=True)

def server_sign_in(username, password, site_content_url = None):
    """ Sign into Tableau Server via REST API """
    url = '{}/api/{}/auth/signin'.format(TS_BASE_URL, TS_API_VERSION)
    headers = {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
    }
    payload = {
        'credentials': {
            'name': username,
            'password': password,
            'site': {
                'contentUrl': '' if site_content_url is None else site_content_url
            }
        }
    }
    r = requests.post(url, headers=headers, json=payload)
    response = r.json()
    return (response['credentials']['token'], response['credentials']['site']['id'])

def server_sign_out(token):
    """ Sign out of Tableau Server and invalidate the token. """
    url = '{}/api/{}/auth/signout'.format(TS_BASE_URL, TS_API_VERSION)
    headers = {
        'X-Tableau-Auth': token
    }
    r = requests.post(url, headers=headers)
    if r.status_code != 204:
        raise ValueError('There may have been an error while signing out.')

def make_multipart(parts):
    """ Handle multipart POST message. """
    mime_multipart_parts = []
    for name, (filename, blob, content_type) in parts.items():
        multipart_part = RequestField(name=name, data=blob, filename=filename)
        multipart_part.make_multipart(content_type=content_type)
        mime_multipart_parts.append(multipart_part)

    post_body, content_type = encode_multipart_formdata(mime_multipart_parts)
    content_type = ''.join(('multipart/mixed',) + content_type.partition(';')[1:])
    return post_body, content_type

def publish_data_source(path_to_tds, token, site_id, project_id):
    """ Push a TDS file to Tableau Server """
    tds_size = os.path.getsize(path_to_tds)
    tds_name = os.path.basename(path_to_tds)
    tds_filename, tds_extension = tds_name.split('.', 1)
    if tds_size > FILESIZE_LIMIT:
        # If you are generating large files (over 64MB), you'll need to split into
        # chunks and push the file in pieces. This isn't likely to apply since
        # we are creating a TDS that connects to a DB so there's no embedded
        # data to worry about.
        pass
    else:
        with open(path_to_tds, 'rb') as f:
            tds_bytes = f.read()

        request_payload = {
            'datasource': {
                'name': tds_name,
                'connectionCredentials': {
                    'name': DB_USER,
                    'password': DB_PASS,
                    'embed': True
                },
                'project': {
                    'id': project_id
                }
            }
        }
        parts = {
            'request_payload': ('', json.dumps(request_payload), 'application/json'),
            'tableau_datasource': (tds_name, tds_bytes, 'application/octet-stream')
        }
        payload, content_type = make_multipart(parts)
        url = '{}/api/{}/sites/{}/datasources'.format(TS_BASE_URL, TS_API_VERSION, site_id)
    params = {
        'datasourceType': tds_extension,
        'overwrite': True
    }
    headers = {
        'X-Tableau-Auth': token,
        'Content-Type': content_type,
        'Accept': 'application/json'
    }
    r = requests.post(url, data=payload, headers=headers, params=params)
    if r.status_code != 201:
        message = 'There was an issue publishing your data source. The server responded with: \n\n{}'.format(r.json())
        raise ValueError(message)

# write the TDS, login to Tableau Server, publish the TDS, logout
print('Writing TDS file...')
write_tds(DB_SERVER, DB_PORT, DB_NAME, DB_USER, DB_CLASS, './tds/foo.tds')
print('Logging into Tableau Server...')
token, site_id = server_sign_in(TS_USER, TS_PASS, 'RESTAPIDataSourcePublish')
print('Publishing TDS to Tableau File...')
publish_data_source('./tds/foo.tds', token, site_id, PROJ_ID)
print('Signing out of Tableau Server...')
server_sign_out(token)
